# README #

This module should be only used by drupal developer.

### How do I get set up? ###

* Install [**Entity**](https://www.drupal.org/project/entity) module
* A good undestanding of [**EntityMetadataWrapper**](http://www.mediacurrent.com/blog/entity-metadata-wrapper) will be needed as well

### Who do I talk to? ###

* Drupal developer

### Why not [**feeds**](https://www.drupal.org/project/feeds)? ###

* The support of feed for all entity type still poor (on my point of view)
* I have more control over my fields

**All contributions are welcome**